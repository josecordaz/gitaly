module gitlab.com/gitlab-org/gitaly

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/cloudflare/tableflip v0.0.0-20190329062924-8392f1641731
	github.com/getsentry/raven-go v0.1.2
	github.com/golang/protobuf v1.3.1
	github.com/google/uuid v1.1.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/go-grpc-prometheus v1.2.0
	github.com/kelseyhightower/envconfig v1.3.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/libgit2/git2go v0.0.0-20190104134018-ecaeb7a21d47
	github.com/prometheus/client_golang v1.0.0
	github.com/sirupsen/logrus v1.2.0
	github.com/stretchr/testify v1.3.0
	github.com/tinylib/msgp v1.1.0 // indirect
	gitlab.com/gitlab-org/labkit v0.0.0-20190221122536-0c3fc7cdd57c
	golang.org/x/crypto v0.0.0-20190530122614-20be4c3c3ed5 // indirect
	golang.org/x/net v0.0.0-20190613194153-d28f0bde5980
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894
	google.golang.org/genproto v0.0.0-20181202183823-bd91e49a0898 // indirect
	google.golang.org/grpc v1.16.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
